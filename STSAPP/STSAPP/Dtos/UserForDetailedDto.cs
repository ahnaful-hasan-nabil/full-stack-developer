using System;
using System.Collections.Generic;

namespace STSAPP.Dtos
{
    public class UserForDetailedDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string FullName { get; set; }
        public DateTime Created { get; set; }
    }
}