using System;
using System.ComponentModel.DataAnnotations;

namespace STSAPP.Dtos
{
    public class UserForUpdateDto
    {
        [Required]
        public string FullName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
    }
}