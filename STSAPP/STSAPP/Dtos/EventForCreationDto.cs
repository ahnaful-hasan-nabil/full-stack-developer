﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STSAPP.Dtos
{
    public class EventForCreationDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Location { get; set; }
        public int NotifyBeforeMinutes { get; set; }
        public int NotificationTypeId { get; set; }
    }
}
