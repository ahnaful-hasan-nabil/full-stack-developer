﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STSAPP.Data;
using STSAPP.Dtos;
using STSAPP.Models;

namespace STSAPP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EventsController : ControllerBase
    {
        private readonly IEventRepository _repo;
        private readonly IMapper _mapper;
        public EventsController(IEventRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        [HttpGet("{userId}", Name = "GetEvents")]
        public async Task<IActionResult> GetEvents(int userId)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var eventsFromRepo = await _repo.GetEvents(userId);

            if (eventsFromRepo == null)
                return NotFound();

            return Ok(eventsFromRepo);
        }

        [HttpGet("{id}", Name = "GetEvent")]
        public async Task<IActionResult> GetEvents(int userId, int id)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var eventFromRepo = await _repo.GetEvent(id);

            if (eventFromRepo == null)
                return NotFound();

            return Ok(eventFromRepo);
        }

        [HttpPost]
        public async Task<IActionResult> CreateEvent(int userId, EventForCreationDto eventForCreationDto)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var @event = _mapper.Map<Event>(eventForCreationDto);
            @event.UserId = userId;
            _repo.Add(@event);

            if (await _repo.SaveAll())
            {
                var eventToReturn = _mapper.Map<EventToReturn>(@event);
                return CreatedAtRoute("GetEvent", new { userId, id = @event.Id }, eventToReturn);
            }

            throw new Exception("Creating the event failed on save");
        }
    }
}
