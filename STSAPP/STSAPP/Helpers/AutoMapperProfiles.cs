using System.Linq;
using AutoMapper;
using STSAPP.Dtos;
using STSAPP.Models;

namespace STSAPP.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, UserForListDto>();
            CreateMap<User, UserForDetailedDto>();
            CreateMap<UserForUpdateDto, User>();
            CreateMap<UserForRegisterDto, User>()
                .ForMember(dest => dest.UserName, opt =>
                    opt.MapFrom(src => src.Email));
            CreateMap<EventForCreationDto, Event>();
            CreateMap<Event, EventToReturn>();
        }
    }
}