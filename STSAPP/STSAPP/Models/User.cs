using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace STSAPP.Models
{
    public class User : IdentityUser<int>
    {
        public DateTime DateOfBirth { get; set; }
        public string FullName { get; set; }
        public DateTime Created { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
        public virtual ICollection<Event> Events { get; set; }
    }
}