using Microsoft.AspNetCore.Identity;

namespace STSAPP.Models
{
    public class UserRole : IdentityUserRole<int>
    {
        public virtual User User { get; set; }
        public virtual Role Role { get; set; }
    }
}