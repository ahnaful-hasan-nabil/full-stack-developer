﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STSAPP.Models
{
    public class Event
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string Location { get; set; }
        public int NotifyBeforeMinutes { get; set; }
        public int NotificationTypeId { get; set; }
        public int UserId { get; set; }

        public virtual NotificationType NotificationType { get; set; }
        public virtual User User { get; set; }
    }
}
