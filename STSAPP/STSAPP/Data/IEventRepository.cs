using System.Collections.Generic;
using System.Threading.Tasks;
using STSAPP.Helpers;
using STSAPP.Models;

namespace STSAPP.Data
{
    public interface IEventRepository
    {
        void Add<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveAll();
        Task<List<Event>> GetEvents(int userId);
        Task<Event> GetEvent(int id);
        Task<User> GetUser(int id);
    }
}