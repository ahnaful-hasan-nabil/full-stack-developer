using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using STSAPP.Helpers;
using STSAPP.Models;
using Microsoft.EntityFrameworkCore;

namespace STSAPP.Data
{
    public class EventRepository : IEventRepository
    {
        private readonly DataContext _context;
        public EventRepository(DataContext context)
        {
            _context = context;
        }

        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<List<Event>> GetEvents(int userId)
        {
            var events = await _context.Events.Where(e => e.UserId == userId).ToListAsync();
            return events;
        }

        public async Task<Event> GetEvent(int id)
        {
            var @event = await _context.Events.FirstOrDefaultAsync(e => e.Id == id);
            return @event;
        }

        public async Task<User> GetUser(int id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);

            return user;
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}