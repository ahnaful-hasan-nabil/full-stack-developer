import { Time } from '@angular/common';

export interface Event{
  id: number;
  title: string;
  description: string;
  date: Date;
  from: Time;
  to: Time;
  location: string;
  notifyBeforeMinutes: number;
  notificationTypeId: number;
  userId: number;
}
